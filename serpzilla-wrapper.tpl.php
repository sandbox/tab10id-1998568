<?php
/**
 * @file
 * Default theme implementation to wrap menu blocks.
 *
 * Available variables:
 * - $content: The renderable array containing the menu.
 * - $classes: A string containing the CSS classes for the DIV tag. Includes:
 *   serpzilla-block-DELTA
 * - $classes_array: An array containing each of the CSS classes.
 *
 * The following variables are provided for contextual information.
 * - $delta: (string) The serpzilla_block's block delta.
 * - $config: An array of the block's configuration settings. Includes
 *   title_link, admin_title, max_links and list_style.
 *
 * @see template_preprocess_serpzilla_block_wrapper()
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($content); ?>
</div>
