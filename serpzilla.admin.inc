<?php

/**
 * @file
 * Provides infrequently used functions and hooks for serpzilla.
 */

/**
 * Implements hook_theme().
 */
function _serpzilla_theme(&$existing, $type, $theme, $path) {
  return array(
    'serpzilla_wrapper' => array(
      'template' => 'serpzilla-wrapper',
      'variables' => array('content' => array(), 'config' => array(), 'delta' => NULL),
      'pattern' => 'serpzilla_wrapper__',
    ),
    'serpzilla_links_inline' => array(
      'variables' => array('links' => array()),
    ),
  );
}

function serpzilla_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $form = block_admin_configure($form, $form_state, 'serpzilla', NULL);

  // Other modules should be able to use hook_form_block_add_block_form_alter()
  // to modify this form, so add a base form ID.
  $form_state['build_info']['base_form_id'] = 'block_add_block_form';

  // Prevent block_add_block_form_validate/submit() from being automatically
  // added because of the base form ID by providing these handlers manually.
  $form['#validate'] = array();
  $form['#submit'] = array('serpzilla_add_block_form_submit');

  return $form;
}

/**
 * Save the new menu block.
 */
function serpzilla_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('serpzilla_block_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;
  $form_state['values']['delta'] = $delta;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  variable_set('serpzilla_block_ids', $block_ids);

  // Save the block configuration.
  serpzilla_block_save($delta, $form_state['values']);

  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'region', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'status' => 0,
        'status' => (int) ($region != BLOCK_REGION_NONE),
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Implements hook_block_save().
 */
function _serpzilla_block_save($delta = '', $edit = array()) {
  if (!empty($delta)) {
    variable_set("serpzilla_block_{$delta}_admin_title", $edit['admin_title']);
    variable_set("serpzilla_block_{$delta}_max_links", $edit['max_links']);
    variable_set("serpzilla_block_{$delta}_list_style", $edit['list_style']);
  }
}

/**
 * Menu callback: confirm deletion of menu blocks.
 */
function serpzilla_block_delete($form, &$form_state, $delta = 0) {
  $title = _serpzilla_block_format_title(serpzilla_block_get_config($delta));
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Deletion of menu blocks.
 */
function serpzilla_block_delete_submit($form, &$form_state) {
  // Remove the menu block configuration variables.
  $delta = $form_state['values']['delta'];
  $block_ids = variable_get('serpzilla_block_ids', array());
  unset($block_ids[array_search($delta, $block_ids)]);
  sort($block_ids);
  variable_set('serpzilla_block_ids', $block_ids);
  variable_del("serpzilla_block_{$delta}_admin_title");
  variable_del("serpzilla_block_{$delta}_max_links");
  variable_del("serpzilla_block_{$delta}_list_style");

  db_delete('block')
    ->condition('module', 'serpzilla')
    ->condition('delta', $delta)
    ->execute();
  db_delete('block_role')
    ->condition('module', 'serpzilla')
    ->condition('delta', $delta)
    ->execute();
  drupal_set_message(t('The block "%name" has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}

/**
 * Implements hook_block_info().
 */
function _serpzilla_block_info() {
  $blocks = array();
  $deltas = variable_get('serpzilla_block_ids', array());
  foreach (array_keys(module_invoke_all('serpzilla_blocks')) as $delta) {
    $deltas[] = $delta;
  }
  foreach ($deltas AS $delta) {
    $blocks[$delta]['info'] = 'Serpzilla: ' . _serpzilla_block_format_title(serpzilla_block_get_config($delta));
  }
  $blocks[$delta]['cache'] = DRUPAL_NO_CACHE;
  return $blocks;
}

/**
 * Return the title of the block.
 *
 * @param $config
 *   array The configuration of the menu block.
 * @return
 *   string The title of the block.
 */
function _serpzilla_block_format_title($config) {
  // If an administrative title is specified, use it.
  if (!empty($config['admin_title'])) {
    return check_plain($config['admin_title']);
  }
  $title = t('Adv');
  return $title;
}

/**
 * Implements hook_block_configure().
 */
function _serpzilla_block_configure($delta = '') {
  // Create a pseudo form state.
  $form_state = array('values' => serpzilla_block_get_config($delta));
  return serpzilla_block_configure_form(array(), $form_state);
}

/**
 * Returns the configuration form for a menu tree.
 *
 * @param $form_state
 *   array An associated array of configuration options should be present in the
 *   'values' key. If none are given, default configuration is assumed.
 * @return
 *   array The form in Form API format.
 */
function serpzilla_block_configure_form($form, &$form_state) {
  $config = array();
  // Get the config from the form state.
  if (!empty($form_state['values'])) {
    $config = $form_state['values'];
  }
  // Merge in the default configuration.
  $config += serpzilla_block_get_config();

  // Build the standard form.
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => $config['admin_title'],
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
  );
  $form['max_links'] = array(
    '#type' => 'select',
    '#title' => t('Max links quantity'),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('1'),
      2 => t('2'),
      3 => t('3'),
      4 => t('4'),
      5 => t('5'),
      6 => t('All remaining'),
    ),
    '#default_value' => $config['max_links'],
  );
  $form['list_style'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show as list'),
    '#default_value' => $config['list_style'],
  );

  return $form;
}

/**
 * Menu callback: admin settings form.
 *
 * @return
 *   The settings form used by Menu block.
 */
function serpzilla_admin_settings_form($form, &$form_state) {
  $form['serpzilla_uid'] = array(
    '#type' => 'textfield',
    '#title' => t('Serpzilla UID'),
    '#description' => t('if you don`t know your UID take it !url', array('!url' => l(t('here'), 'https://www.serpzilla.com/en/wm/sites/addwizard/'))),
    '#default_value' => variable_get('serpzilla_uid', ''),
    '#size' => 32,
    '#maxlength' => 32,
  );
  $form['#validate'] = array('serpzilla_config_validate_uid');
  return system_settings_form($form);
}

function serpzilla_config_validate_uid($form, &$form_state) {
  if (strlen(trim($form_state['values']['serpzilla_uid'])) != 32) {
    form_set_error('serpzilla_uid', 'длина UID - 32 символа');
  }
}

/**
 * Form submission handler.
 */
function serpzilla_admin_settings_form_submit($form, &$form_state) {
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Alters the block admin form to add delete links next to menu blocks.
 */
function _serpzilla_form_block_admin_display_form_alter(&$form, $form_state) {
  $blocks = module_invoke_all('serpzilla_blocks');
  foreach (variable_get('serpzilla_block_ids', array()) AS $delta) {
    if (empty($blocks[$delta])) {
      $form['blocks']['serpzilla_' . $delta]['delete'] = array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => 'admin/structure/block/delete-serpzilla-block/' . $delta,
      );
    }
  }
}